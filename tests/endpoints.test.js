describe('test', () => {
  it('should return a string', () => {
    expect('Hello World!').toBe('Hello World!');
  });
});

/*
const axios = require('axios')

describe('tests for calling endpoints', () => {

  describe('tests for /messages', () => {
    it('should respond with status 200', (done) => {
      
      axios.get('http://localhost:8082/messages')

      .then((res) => {
        
        expect(res.status).toBe(200)
        done()

      })
    })
  })

  describe('tests for /node-statistic', () => {
    it('should respond with status 200', (done) => {
      
      axios.get('http://localhost:8082/node-statistic')

      .then((res) => {
        
        expect(res.status).toBe(200)
        done()

      })
    })
  })

  describe('tests for /queue-statistic', () => {
    it('should respond with status 200', (done) => {
      
      axios.get('http://localhost:8082/queue-statistic')

      .then((res) => {
        
        expect(res.status).toBe(200)
        done()

      })
    })
  })

  describe('tests for /state', () => {
    it('should respond with status 200', (done) => {
      
      axios.get('http://localhost:8084/state')

      .then((res) => {
        
        expect(res.status).toBe(200)
        done()

      })
    })
  })
    
})
*/