const amqp = require("amqplib");


const sub = async () => {
    //made connection with rabbitmq amqp://{hostname}
    const connection = await amqp.connect('amqp://rabbitmq')
    //creating new channel after connection created
    const channel = await connection.createChannel();

    const exchange = 'topic_logs';
    const key_out = 'my.o';
    const key_in = 'my.i';

    //check exchage is available with topic mode
    channel.assertExchange(exchange, 'topic', {
        durable: true
    })
    //creating a queue
    const q = channel.assertQueue('', {exclusive: true})
    //binding queue with exchange
    channel.bindQueue(q.queue, exchange, key_out)
    
    //consuming from queue binded with exchange
    channel.consume(q.queue, msg => {
        //wait 1 second and then publish incoming msg
        setTimeout(()=>{
            const incoming = msg.content;
            const newMsg = 'Got ' + incoming
            channel.publish(exchange, key_in, Buffer.from(newMsg));
            console.log(" [x] Sent %s:'%s'", key_in, newMsg);
        }, 1000);

    

    }, {noAck:true})
}
//Sub() function will be called after 10 seconds
setTimeout( () => {sub()}, 10000);