const express = require('express')
const app = express()
const fs = require('fs');
const PORT = 8082;
const http = require("http");

//GET request to localhost:8082/messages
app.get('/messages', (req, res) => {
    //read file saved by observe
    fs.readFile('./messages/messages.txt', (err, data) => {

        if(err){
            throw err;
        }
        //setting header and status code for response
        res.writeHead(200, {'Content-Type': 'text/plain'})
        //sending response data
        res.write(data)
        //ending response
        res.end()
    })
})
//GET request to localhost:8082/node-statistc
app.get('/node-statistic', async (req, res) =>  {
    //http GET method to get statistic of all nodes
    http.get('http://guest:guest@rabbitmq:15672/api/nodes', (onResponse) => {
        
        let data = ''
        //adding chunk data
        onResponse.on('data', (chunk) => {
            data += chunk
        })
        //after response data receive ends
        onResponse.on('end', () => {
            //parsing data in JSON object
            data = JSON.parse(data)
    
            let newArray = [
                {"mem_used": data[0].mem_used},
                {"mem_limit": data[0].mem_limit},
                {"fd_total": data[0].fd_total},
                {"fd_used": data[0].fd_used},
                {"sockets_total": data[0].sockets_total},
                {"sockets_used": data[0].sockets_used}
            ]
             //setting header and status code for response
            res.writeHead(200, 'Content-Type', 'application/json')
            //converting JSON object to string
            res.write(JSON.stringify(newArray))
            //ending response
            res.end()
        })
    })
})
//GET request to localhost:8082/queue-statistic
app.get('/queue-statistic', async (req, res) =>  {
    http.get('http://guest:guest@rabbitmq:15672/api/queues', (onResponse) => {
        
        let data = ''
        //adding chunk data
        onResponse.on('data', (chunk) => {
            data += chunk
        })
        //after response data receive ends
        onResponse.on('end', () => {
            //parsing data in JSON object
            data = JSON.parse(data)
            
            let newArray= [];
            //pushing message_stats of all queues
            for(i=0; i<data.length; i++){
                newArray.push(data[i].message_stats);
            }
            
            //setting header and status code for response
            res.writeHead(200, 'Content-Type', 'application/json')
            //converting JSON object to string
            res.write(JSON.stringify(newArray))
            //ending response
            res.end()
        })
    })
})


//listening to the server
app.listen(PORT, (req, res) => {
    console.log(`Server running on ${PORT}`)
})