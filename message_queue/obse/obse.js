const amqp = require('amqplib');
const fs = require('fs');


const obse = async () => {

    //made connection with rabbitmq amqp://{hostname}
    const connection = await amqp.connect('amqp://rabbitmq')
    //creating new channel after connection created
    const channel = await connection.createChannel();

    const exchange = 'topic_logs';
    
    //check exchage is available with topic mode
    channel.assertExchange(exchange, 'topic', {
        durable: true
    })
    //creating a queue
    const q = channel.assertQueue('', {exclusive: true})
    //wild card subscription for all keys within exchange
    channel.bindQueue(q.queue, exchange, '#')
    
    try{
        //unlinkSync() remove file from given path
        fs.unlinkSync('./messages/messages.txt');
    } catch(error) {
        console.log(error)
    }
    //consuming from queue binded with exchange
    channel.consume(q.queue, msg => {

                const incoming = `${new Date().toISOString()} Topic ${msg.fields.routingKey}: ${msg.content}`;
                //append data to mentioned file
                fs.appendFile('./messages/messages.txt', incoming+'\n', (err) => {
                    if(err) {
                        return console.log(err);
                    }
                    console.log('Incoming: ', incoming);
                })

    }, {noAck:true})
}
//Obse() function will be called after 10 seconds
setTimeout( () => {obse()}, 10000);