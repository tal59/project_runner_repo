const amqp = require("amqplib");
const express = require('express')
const app = express()
const PORT = 8084
const bodyParser = require('body-parser')

const textParser = bodyParser.text()

let state = 'INIT'
let stateValue = ''

app.get('/state', (req, res) => {
    res.writeHead(200, 'Content-Type', 'text/plain')
    res.write(state)
    res.end()
})

app.put('/state',textParser ,(req, res) => {
    const body = req.body
    publish(body)
    res.send('200')
})

let channel = ''
var exchange = 'topic_logs';
var key = 'my.o'

const Initialize = async () => {

    //made connection with rabbitmq amqp://{hostname}
    const connection = await amqp.connect('amqp://rabbitmq')
    
    //creating new channel after connection created
    channel = await connection.createChannel();
    
    //check exchage is available with topic mode
    channel.assertExchange(exchange, 'topic', {
        durable: true
    })
}

const Running = () => {

    //publishing message after 3 seconds
    for(i=1; i<15; i++){
        console.log('inside while loop')
        let msg = 'MSG_' +i;
        channel.publish(exchange, key, Buffer.from(msg));
        console.log(" [x] Sent %s:'%s'", key, msg);
        setTimeout(() => {}, 3000);
    }
}

const Shutdown = () => {

    //closing connection
    connection.close();
    process.exit(0)
}

const publish = async (stateValue) => {
    
    if(stateValue == 'INIT'){
        state = 'INIT'
        Initialize()
    }
    else if(stateValue == 'RUNNING'){
        state = 'RUNNING'
        Running()
    }
    else if(stateValue == 'SHUTDOWN'){
        state = 'SHUTDOWN'
        Shutdown()
    }
    else{
        state = 'PAUSED'
    }
}
//Publish() function will be called after 1 seconds
setTimeout(() => {
    publish();
}, 1000);

app.listen(PORT, (req, res) => {
    console.log(`ORIGI runnig on ${PORT}`)
})

